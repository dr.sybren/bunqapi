module gitlab.com/dr.sybren/bunqapi

require (
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.2.2
)
