# Bunq API implementation

This is the start of my [bunq API](https://doc.bunq.com/) implementation in Go.
It's not fully featured yet, but it supports:

- Installation requests, including auto-generating RSA keys.
- Signing of requests, and verification of server responses.
- Device registration.
- Starting & stopping a session.
- Creating & listing bank accounts.

Note that this library is intended for simple, short-running applications. It has no proper error handling, and can simply exit the process upon an error.


## Author

This code has been created by [dr. Sybren A. Stüvel](https://stuvel.eu/) as a hobby project.

## License

This software is Open Source, and is covered by the MIT License as available in [LICENSE.txt](LICENSE.txt).
