/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"
)

func writePEM(filename, pemType string, pemBytes []byte) {
	log := log.WithField("filename", filename)

	pemdata := pem.EncodeToMemory(
		&pem.Block{
			Type:  pemType,
			Bytes: pemBytes,
		},
	)
	log.Debug("writing PEM file")
	err := ioutil.WriteFile(filename, pemdata, 0600)
	if err != nil {
		log.WithError(err).Fatal("unable to write file")
	}
}

func readPEM(filename, pemType string) ([]byte, error) {
	log := log.WithField("filename", filename)

	pemdata, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var block *pem.Block
	for {
		block, pemdata = pem.Decode(pemdata)
		if block == nil {
			log.WithField("expectedHeader", pemType).Fatal("unable to find expected block of PEM data")
		}
		if block.Type == pemType {
			return block.Bytes, nil
		}
	}
}

func parsePEM(pemString, pemType string) ([]byte, error) {
	pemdata := []byte(pemString)
	var block *pem.Block
	for {
		block, pemdata = pem.Decode(pemdata)
		if block == nil {
			log.WithField("expectedHeader", pemType).Panic("unable to find expected block of PEM data")
		}
		if block.Type == pemType {
			return block.Bytes, nil
		}
	}

}

func generateNewPrivateKey() *rsa.PrivateKey {
	privkey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log.WithError(err).Fatal("unable to generate new RSA key")
	}
	writePEM(fileRSAPrivateKey,
		"RSA PRIVATE KEY",
		x509.MarshalPKCS1PrivateKey(privkey))

	return privkey
}

func ensureOwnRSAKey() *rsa.PrivateKey {
	privlog := log.WithField("filename", fileRSAPrivateKey)

	keyBytes, err := readPEM(fileRSAPrivateKey, "RSA PRIVATE KEY")
	if err != nil {
		if os.IsNotExist(err) {
			privlog.WithError(err).Info("unable to load private key, going to generate new one")
			generateNewPrivateKey()
			return ensureOwnRSAKey()
		}
		privlog.WithError(err).Fatal("unable to load private RSA key file")
	}

	privkey, err := x509.ParsePKCS1PrivateKey(keyBytes)
	if err != nil {
		privlog.WithError(err).Fatal("unable to parse private RSA key")
	}
	privlog.WithField("keySizeBits", privkey.Size()*8).Debug("loaded private RSA key")

	if err := privkey.Validate(); err != nil {
		privlog.WithError(err).Fatal("RSA key could be loaded but is invalid")
	}

	return privkey
}

func parsePublicRSAKeyString(pemString string) *rsa.PublicKey {
	keyBytes, err := parsePEM(pemString, "PUBLIC KEY")
	if err != nil {
		log.WithError(err).Error("unable to parse public key, assuming it's not there")
		return nil
	}

	pubkey, err := x509.ParsePKIXPublicKey(keyBytes)
	if err != nil {
		log.WithError(err).Fatal("unable to parse public RSA key")
	}

	rsaPubkey, ok := pubkey.(*rsa.PublicKey)
	if !ok {
		log.Fatal("public key is not an RSA key")
	}
	log.WithField("keySizeBits", rsaPubkey.Size()*8).Debug("loaded public RSA key")

	return rsaPubkey
}
