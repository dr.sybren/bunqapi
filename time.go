/* (c) 2019 David Stotijn, dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"fmt"
	"strings"
	"time"
)

// Time represents a parsed timestamp returned by the bunq API.
type Time time.Time

// UnmarshalJSON is being used to parse timestamps from the bunq API.
// Bunq timestamps come into this format: "2015-06-13 23:19:16.215235"
func (ariTime *Time) UnmarshalJSON(input []byte) error {
	strInput := string(input)
	strInput = strings.Trim(strInput, `"`)
	var pattern string
	if len(strInput) == 10 {
		pattern = "2006-01-02"
	} else {
		pattern = "2006-01-02 15:04:05.000000"
	}
	newTime, err := time.Parse(pattern, strInput)
	if err != nil {
		return fmt.Errorf("Error parsing Time: %s", err)
	}
	*ariTime = Time(newTime)

	return nil
}

// MarshalText implements the encoding.MarshalText interface for custom time type.
func (ariTime *Time) MarshalText() ([]byte, error) {
	t := time.Time(*ariTime)
	return []byte(t.Format("2006-01-02 15:04:05.000000")), nil
}
