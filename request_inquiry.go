/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import "fmt"

// RequestInquiry is a request-for-payment (RFP)
type RequestInquiry struct {
	AmountInquired    *Amount               `json:"amount_inquired,omitempty"`
	CounterpartyAlias *LabelMonetaryAccount `json:"counterparty_alias,omitempty"`
	Description       string                `json:"description,omitempty"`
	Attachment        []BunqID              `json:"attachment,omitempty"`
	MerchantReference string                `json:"merchant_reference,omitempty"`
	Status            string                `json:"status,omitempty"`
	MinimumAge        int                   `json:"minimum_age,omitempty"`
	RequireAddress    string                `json:"require_address,omitempty"`
	AllowBunqme       bool                  `json:"allow_bunqme"`
	RedirectURL       string                `json:"redirect_url,omitempty"`
	EventID           int                   `json:"event_id,omitempty"`
	ID                int                   `json:"id,omitempty"`
	Created           string                `json:"created,omitempty"`
	Updated           string                `json:"updated,omitempty"`
	TimeResponded     string                `json:"time_responded,omitempty"`
	TimeExpiry        string                `json:"time_expiry,omitempty"`
	MonetaryAccountID int                   `json:"monetary_account_id,omitempty"`
	AmountResponded   *Amount               `json:"amount_responded,omitempty"`
	UserAliasCreated  *LabelUser            `json:"user_alias_created,omitempty"`
	UserAliasRevoked  *LabelUser            `json:"user_alias_revoked,omitempty"`
	BatchID           int                   `json:"batch_id,omitempty"`
	ScheduledID       int                   `json:"scheduled_id,omitempty"`
	BunqmeShareURL    string                `json:"bunqme_share_url,omitempty"`
	AddressShipping   *Address              `json:"address_shipping,omitempty"`
	AddressBilling    *Address              `json:"address_billing,omitempty"`
	Geolocation       *Geolocation          `json:"geolocation,omitempty"`
	AllowChat         bool                  `json:"allow_chat,omitempty"`

	// Not yet implemented:
	// ReferenceSplitTheBill *RequestReferenceSplitTheBillAnchorObject `json:"reference_split_the_bill,omitempty"`
}

// LabelMonetaryAccount is defined by the bunq API.
type LabelMonetaryAccount struct {
	Type  string `json:"type,omitempty"`
	Value string `json:"value,omitempty"`
	Name  string `json:"name,omitempty"`

	IBAN                      string     `json:"iban,omitempty"`
	Avatar                    *Avatar    `json:"avatar,omitempty"`
	LabelUser                 *LabelUser `json:"label_user,omitempty"`
	Country                   string     `json:"country,omitempty"`
	IsLight                   bool       `json:"is_light,omitempty"`
	SwiftBic                  string     `json:"swift_bic,omitempty"`
	SwiftAccountNumber        string     `json:"swift_account_number,omitempty"`
	TransferwiseAccountNumber string     `json:"transferwise_account_number,omitempty"`
	TransferwiseBankCode      string     `json:"transferwise_bank_code,omitempty"`

	// BunqMe                    *Pointer   `json:"bunq_me,omitempty"`
}

// CreateRequestInquiry creates a payment request.
func (c *Client) CreateRequestInquiry(monetaryAccountID int, rfp RequestInquiry) int {
	wrappedResponse := wrappedIDResponse{}
	url := fmt.Sprintf("user/%d/monetary-account/%d/request-inquiry", c.session.UserPerson.ID, monetaryAccountID)
	errResp := c.DoRequest("POST", url, &rfp, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing request inquiry creation request")
	}

	response := idResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	log.WithField("requestInquiryID", response.ID.ID).Info("payment request created")
	return response.ID.ID
}
