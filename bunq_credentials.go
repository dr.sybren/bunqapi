/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"crypto/rsa"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

// Credentials are stored in YAML and serve as our config.
type Credentials struct {
	filename string
	apiURL   *url.URL

	APIMode string `yaml:"apiMode"` // "production" or "sandbox"
	APIKey  string `yaml:"apiKey"`

	InstallationToken string `yaml:"installationToken,omitempty"`
	ServerPublicKey   string `yaml:"serverPublicKey,omitempty"`
	serverPublicKey   *rsa.PublicKey
	DeviceID          *int `yaml:"deviceID,omitempty"`
}

// LoadCredentials loads credentials from a YAML file.
func LoadCredentials() Credentials {
	filename, err := filepath.Abs(fileCredentials)
	if err != nil {
		log.WithFields(logrus.Fields{
			"filename":      fileCredentials,
			logrus.ErrorKey: err,
		}).Panic("unable to convert filename to absolute path")
	}

	logger := log.WithField("filename", filename)

	credfile, err := ioutil.ReadFile(filename)
	if err != nil {
		logger.WithError(err).Fatal("unable to open credentials file")
	}

	creds := Credentials{filename: filename}
	if err := yaml.Unmarshal(credfile, &creds); err != nil {
		logger.WithError(err).Fatal("unable to decode YAML")
	}

	switch creds.APIMode {
	case "sandbox":
		creds.apiURL, err = url.Parse(urlSandbox)
	case "production":
		creds.apiURL, err = url.Parse(urlProduction)
	default:
		logger.WithField("apiMode", creds.APIMode).Fatal("invalid API mode, should be sandbox or production")
	}
	if err != nil {
		logger.WithField("apiMode", creds.APIMode).Panic("unable to parse URL for this mode")
	}
	logger.WithField("url", creds.apiURL.String()).Debug("set API URL based on API mode")

	creds.updateServerPublicKey()

	return creds
}

func (creds *Credentials) updateServerPublicKey() {
	if creds.ServerPublicKey == "" {
		creds.serverPublicKey = nil
	} else {
		creds.serverPublicKey = parsePublicRSAKeyString(creds.ServerPublicKey)
	}
}

// Endpoint returns the URL for a given endpoint name.
// It takes sandbox/production switching into account.
func (creds *Credentials) Endpoint(endpoint string) string {
	endpointURL, err := creds.apiURL.Parse(endpoint)
	if err != nil {
		log.WithFields(logrus.Fields{
			"apiURL":        creds.apiURL.String(),
			"endpoint":      endpoint,
			logrus.ErrorKey: err,
		}).Panic("unable to parse API endpoint URL")
	}

	return endpointURL.String()
}

// Save stores the credentials in the same file they were read from.
func (creds *Credentials) Save() {
	logger := log.WithField("filename", creds.filename)

	bytes, err := yaml.Marshal(creds)
	if err != nil {
		logger.WithError(err).Fatal("unable to encode YAML")
	}

	tmpname := creds.filename + "~"
	if err := ioutil.WriteFile(tmpname, bytes, 0600); err != nil {
		logger.WithError(err).Fatal("unable to write YAML file")
	}

	if err := os.Remove(creds.filename); err != nil {
		logger.WithError(err).Fatal("unable to remove old YAML file")
	}
	if err := os.Rename(tmpname, creds.filename); err != nil {
		logger.WithFields(logrus.Fields{
			"tmpname":       tmpname,
			logrus.ErrorKey: err,
		}).Fatal("unable to rename just-written config file")
	}
}
