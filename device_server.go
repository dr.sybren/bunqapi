/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

type deviceServerRequest struct {
	// The description of the DeviceServer. This is only for your own reference when reading the DeviceServer again.
	Description string `json:"description"`
	// The API key. You can request an API key in the bunq app.
	Secret string `json:"secret"`
	// An array of IPs (v4 or v6) this DeviceServer will be able to do calls from. These will be linked to the API key.
	PermittedIPs []string `json:"permitted_ips"`
}

type wrappedDeviceServerResponse struct {
	Response []deviceServerResponse `json:"Response"`
}
type deviceServerResponse struct {
	ID *BunqID `json:"Id"`
}

// CheckDeviceServer registers this device with bunq if we don't have a device ID yet.
func (c *Client) CheckDeviceServer(description string, permittedIPs []string) {
	if c.creds.DeviceID != nil {
		log.WithField("deviceID", *c.creds.DeviceID).Debug("device ID is known")
		return
	}
	c.PostDeviceServer(description, permittedIPs)
}

// PostDeviceServer registers this device with bunq
func (c *Client) PostDeviceServer(description string, permittedIPs []string) {
	payload := deviceServerRequest{
		Description:  description,
		Secret:       c.creds.APIKey,
		PermittedIPs: permittedIPs,
	}

	wrappedResponse := wrappedDeviceServerResponse{}
	errResp := c.DoRequest("POST", "device-server", &payload, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing device registration request")
	}
	response := deviceServerResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	c.creds.DeviceID = &response.ID.ID
	log.WithField("id", *c.creds.DeviceID).Info("registered device device ID")
	c.creds.Save()
}
