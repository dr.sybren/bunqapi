/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCents_UnmarshalJSON_Happy(t *testing.T) {
	amt := Amount{}
	err := json.Unmarshal([]byte(`{"value": "123.45", "currency": "EUR"}`), &amt)

	assert.Nil(t, err)
	assert.Equal(t, 12345, amt.Value.Cents())
	assert.Equal(t, "EUR", amt.Currency)
}

func TestCents_UnmarshalJSON_Unhappy(t *testing.T) {
	amt := Amount{}
	assert.NotNil(t, json.Unmarshal([]byte(`{"value": 123.45, "currency": "EUR"}`), &amt))
	assert.NotNil(t, json.Unmarshal([]byte(`{"value": "123.456", "currency": "EUR"}`), &amt))
	assert.NotNil(t, json.Unmarshal([]byte(`{"value": "123", "currency": "EUR"}`), &amt))
}

func TestCents_MarshalJSON_Happy(t *testing.T) {
	amt := Amount{
		Value:    Cents(12345),
		Currency: "EUR",
	}
	asJSON, err := json.Marshal(&amt)
	assert.Nil(t, err)
	assert.Equal(t, `{"value":"123.45","currency":"EUR"}`, string(asJSON))
}

func TestAmount_Comparisons(t *testing.T) {
	a := NewAmount("EUR", 47)
	b := NewAmount("EUR", 327)

	sum, err := a.Add(b)
	assert.Nil(t, err)
	assert.Equal(t, a.Currency, sum.Currency)
	assert.Equal(t, 47+327, sum.Value.Cents())
	assert.Equal(t, 47, a.Value.Cents())
	assert.Equal(t, 327, b.Value.Cents())

	diff, err := a.Sub(b)
	assert.Nil(t, err)
	assert.Equal(t, a.Currency, diff.Currency)
	assert.Equal(t, 47-327, diff.Value.Cents())
	assert.Equal(t, 47, a.Value.Cents())
	assert.Equal(t, 327, b.Value.Cents())

	isLess, err := a.LessThan(b)
	assert.Nil(t, err)
	assert.True(t, isLess)
	isLess, err = b.LessThan(a)
	assert.Nil(t, err)
	assert.False(t, isLess)

	isGreater, err := a.GreaterThan(b)
	assert.Nil(t, err)
	assert.False(t, isGreater)
	isGreater, err = b.GreaterThan(a)
	assert.Nil(t, err)
	assert.True(t, isGreater)

	assert.True(t, a.Equals(a))
	assert.True(t, a.Equals(NewAmount("EUR", 47)))
	assert.False(t, a.Equals(b))
	assert.False(t, a.Equals(NewAmount("HRK", a.Value.Cents())))

	assert.False(t, a.IsNegative())
	assert.False(t, a.IsZero())
	assert.False(t, a.IsNonPositive())

	z := NewAmount("EUR", 0)
	assert.False(t, z.IsNegative())
	assert.True(t, z.IsZero())
	assert.True(t, z.IsNonPositive())

	n := NewAmount("EUR", -30)
	assert.True(t, n.IsNegative())
	assert.False(t, n.IsZero())
	assert.True(t, n.IsNonPositive())
}
