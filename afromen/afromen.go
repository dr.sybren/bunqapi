/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/bunqapi"
)

const (
	applicationName = "Afromen"
	currency        = "EUR"
)

var maxTransfer = bunqapi.NewAmount(currency, 40*100)

var applicationVersion = "set-during-build"

var cliArgs struct {
	quiet               bool
	debug               bool
	version             bool
	transferFromAccount int
	transferToAccount   int
	threshold           int
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&cliArgs.version, "version", false, "Show the application version and exit.")
	flag.IntVar(&cliArgs.transferFromAccount, "from", 0, "Account ID to transfer money from.")
	flag.IntVar(&cliArgs.transferToAccount, "to", 0, "Account ID to transfer money to.")
	flag.IntVar(&cliArgs.threshold, "threshold", 0,
		fmt.Sprintf("The amount (in entire %s) by which the 'from' account balance is over this threshold is transferred.", currency))
	flag.Parse()
}

func configLogging() {
	logfmt := &logrus.TextFormatter{
		FullTimestamp: true,
	}

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	logrus.SetFormatter(logfmt)
	log.SetOutput(logrus.StandardLogger().Writer())

	bunqapi.LogLevel(logrus.GetLevel())
	bunqapi.LogFormatter(logfmt)
}

func main() {
	parseCliArgs()
	configLogging()
	if cliArgs.version {
		logrus.WithField("version", applicationVersion).Info(applicationName)
		return
	}
	logrus.WithField("version", applicationVersion).Infof("starting %s", applicationName)

	creds := bunqapi.LoadCredentials()
	logrus.WithField("apiMode", creds.APIMode).Debug("loaded credentials")

	client := bunqapi.NewClient(creds)
	client.CheckInstallation()
	client.CheckDeviceServer("je moeder", []string{})
	client.SessionStart()
	defer client.SessionStop()

	if cliArgs.transferFromAccount == 0 || cliArgs.transferToAccount == 0 || cliArgs.threshold == 0 {
		logrus.Warn("to or from account ID or threshold not given, going to list bank accounts")
		listBankAccounts(client)
		return
	}

	threshold := bunqapi.NewAmount(currency, cliArgs.threshold*100)
	afromen(client, threshold)
}

func listBankAccounts(client *bunqapi.Client) {
	accounts := client.GetMonetaryAccountBankList()
	for len(accounts) < 2 {
		logrus.Warning("not enough bank accounts, creating one")

		bankAccount := bunqapi.MonetaryAccountBank{
			Currency: "EUR",
		}
		client.CreateMonetaryAccountBank(bankAccount)

		accounts = client.GetMonetaryAccountBankList()
	}
	logrus.WithField("numAccounts", len(accounts)).Info("enough bank accounts found")

	for _, account := range accounts {
		logrus.WithFields(account.AliasFields()).WithFields(logrus.Fields{
			"accountID": account.ID,
			"balance":   account.Balance.String(),
		}).Info("account balance")
	}
}

func afromen(client *bunqapi.Client, threshold bunqapi.Amount) {
	fromAccount, err := client.GetMonetaryAccountBank(cliArgs.transferFromAccount)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"accountID":     cliArgs.transferFromAccount,
			logrus.ErrorKey: err,
		}).Fatal("unable to find source account")
	}
	logrus.WithFields(fromAccount.AliasFields()).WithFields(logrus.Fields{
		"accountID": fromAccount.ID,
		"balance":   fromAccount.Balance.String(),
	}).Info("source account")

	toAccount, err := client.GetMonetaryAccountBank(cliArgs.transferToAccount)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"accountID":     cliArgs.transferFromAccount,
			logrus.ErrorKey: err,
		}).Fatal("unable to find target account")
	}
	logrus.WithFields(toAccount.AliasFields()).WithFields(logrus.Fields{
		"accountID": toAccount.ID,
		"balance":   toAccount.Balance.String(),
	}).Info("destination account")

	logger := logrus.WithField("threshold", threshold.String())
	toTransfer, err := fromAccount.Balance.Sub(threshold)
	if err != nil {
		logger.WithError(err).Fatal("error computing amount to transfer")
	}

	if toTransfer.IsNonPositive() {
		logger.WithField("accountBalance", fromAccount.Balance.String()).Info("balance < threshold, not transferring")
		return
	}

	// Limit to 'maxTransfer'
	if isGreater, err := toTransfer.GreaterThan(maxTransfer); err != nil {
		logrus.WithFields(logrus.Fields{
			"toTransfer":    toTransfer.String(),
			"maxTransfer":   maxTransfer.String(),
			logrus.ErrorKey: err,
		}).Panic("unable to compare toTransfer with maxTransfer")
	} else if isGreater {
		logrus.WithFields(logrus.Fields{
			"difference": toTransfer.String(),
			"toTransfer": maxTransfer.String(),
		}).Info("limiting how much is transferred")
		toTransfer = maxTransfer
	}

	logger = logger.WithFields(logrus.Fields{
		"toTransfer":  toTransfer.String(),
		"fromBalance": fromAccount.Balance.String(),
		"toBalance":   toAccount.Balance.String(),
	})
	logger.Info("going to transfer money")

	toAlias := toAccount.IBANAlias()
	payment := bunqapi.Payment{
		Amount:      &toTransfer,
		Description: "automatisch afromen",
		CounterpartyAlias: &bunqapi.LabelMonetaryAccount{
			Type:  toAlias.Type,
			Value: toAlias.Value,
			Name:  "je moeder",
		},
	}
	client.CreatePayment(fromAccount.ID, payment)
}
