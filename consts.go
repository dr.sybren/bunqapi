/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

const libraryVersion = "0.0.1"

// URLs of the bunq API.
const (
	urlSandbox = "https://public-api.sandbox.bunq.com/v1/"
	// urlSandbox    = "http://localhost:4444/v1/"
	urlProduction = "https://api.bunq.com/v1/"
)

// Filenames used by the bunq API.
const (
	fileCredentials   = "bunq_credentials.yaml"
	fileRSAPublicKey  = "bunq_key_public.pem"
	fileRSAPrivateKey = "bunq_key_private.pem"
)

// HTTP headers
const (
	headerAccept                     = "Accept"
	headerCacheControl               = "Cache-Control"
	headerContentType                = "Content-Type"
	headerUserAgent                  = "User-Agent"
	headerXBunqAttachmentDescription = "X-Bunq-Attachment-Description"
	headerXBunqClientAuthentication  = "X-Bunq-Client-Authentication"
	headerXBunqClientRequestID       = "X-Bunq-Client-Request-Id"
	headerXBunqClientResponseID      = "X-Bunq-Client-Response-Id"
	headerXBunqClientSignature       = "X-Bunq-Client-Signature"
	headerXBunqGeolocation           = "X-Bunq-Geolocation"
	headerXBunqLanguage              = "X-Bunq-Language"
	headerXBunqRegion                = "X-Bunq-Region"
	headerXBunqServerSignature       = "X-Bunq-Server-Signature"
)

// Default header values
const (
	headerAcceptJSON         = "application/json"
	headerCacheControlNone   = "no-cache"
	headerGeolocationZero    = "0 0 0 0 NL"
	headerLanguageEnglish    = "en_GB"
	headerLanguageNederlands = "nl_NL"
	headerRegionNL           = "nl_NL"
	headerUserAgentAPI       = "bunqapi-golang/" + libraryVersion
)
