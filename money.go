/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// Errors returned by the Amount functions.
var (
	ErrInvalidJSONString = errors.New("Invalid JSON, field must be a string \"xxx.cc\"")
	ErrMixedCurrency     = errors.New("amounts have different currencies")
)

// Amount describes a monetary amount.
type Amount struct {
	// The amount formatted to two decimal places.
	Value Cents `json:"value"`
	// The currency of the amount. It is an ISO 4217 formatted currency code.
	Currency string `json:"currency"`
}

// Cents represents an integer number of cents (1/100th unit of currency).
type Cents int

// NewAmount creates a new amount of integer cents.
func NewAmount(currency string, cents int) Amount {
	return Amount{
		Currency: currency,
		Value:    Cents(cents),
	}
}

// Add returns the sum. Currencies must match.
func (amt Amount) Add(other Amount) (sum Amount, err error) {
	if amt.Currency != other.Currency {
		err = ErrMixedCurrency
		return
	}

	sum = Amount{
		Currency: amt.Currency,
		Value:    amt.Value.Add(other.Value),
	}
	return
}

// Sub returns the difference. Currencies must match.
func (amt Amount) Sub(other Amount) (sum Amount, err error) {
	if amt.Currency != other.Currency {
		err = ErrMixedCurrency
		return
	}

	sum = Amount{
		Currency: amt.Currency,
		Value:    amt.Value.Sub(other.Value),
	}
	return
}

func (amt Amount) cmp(other Amount) (int, error) {
	if amt.Currency != other.Currency {
		return 0, ErrMixedCurrency
	}

	diff := amt.Value.Sub(other.Value).Cents()
	switch {
	case diff < 0:
		return -1, nil
	case diff > 0:
		return 1, nil
	default:
		return 0, nil
	}
}

// LessThan returns true iff amt < other
func (amt Amount) LessThan(other Amount) (bool, error) {
	cmp, err := amt.cmp(other)
	if err != nil {
		return false, err
	}
	return cmp < 0, nil
}

// GreaterThan returns true iff amt > other
func (amt Amount) GreaterThan(other Amount) (bool, error) {
	cmp, err := amt.cmp(other)
	if err != nil {
		return false, err
	}
	return cmp > 0, nil
}

// Equals returns true if both amounts are equal in currency and value.
func (amt Amount) Equals(other Amount) bool {
	return amt.Currency == other.Currency && amt.Value == other.Value
}

func (amt Amount) String() string {
	return fmt.Sprintf("%s %s", amt.Currency, amt.Value.String())
}

// IsNegative returns true iff the amount is negative.
func (amt Amount) IsNegative() bool {
	return amt.Value.Cents() < 0
}

// IsZero returns true iff the amount is zero.
func (amt Amount) IsZero() bool {
	return amt.Value.Cents() == 0
}

// IsNonPositive returns true iff the amount is non-positive.
func (amt Amount) IsNonPositive() bool {
	return amt.Value.Cents() <= 0
}

// Cents returns the number of cents as int.
func (c Cents) Cents() int {
	return int(c)
}

// Add returns the sum
func (c Cents) Add(other Cents) Cents {
	return Cents(int(c) + int(other))
}

// Sub returns the difference
func (c Cents) Sub(other Cents) Cents {
	return Cents(int(c) - int(other))
}

func (c Cents) String() string {
	cents := int(c)
	return fmt.Sprintf("%d.%02d", cents/100, cents%100)
}

// UnmarshalJSON converts a "xxx.cc" string into entire cents.
func (c *Cents) UnmarshalJSON(input []byte) error {
	inLen := len(input)
	if inLen < 2 || input[0] != '"' || input[inLen-1] != '"' {
		return ErrInvalidJSONString
	}

	asString := string(input[1 : inLen-1])
	parts := strings.Split(asString, ".")
	if len(parts) != 2 {
		return ErrInvalidJSONString
	}

	wholeUnits, err := strconv.Atoi(parts[0])
	if err != nil {
		return err
	}
	cents, err := strconv.Atoi(parts[1])
	if err != nil {
		return err
	}
	if cents >= 100 {
		return ErrInvalidJSONString
	}

	*c = Cents(wholeUnits*100 + cents)
	return nil
}

// MarshalJSON converts a Cents object to a "xxx.cc" string.
func (c Cents) MarshalJSON() ([]byte, error) {
	return []byte(`"` + c.String() + `"`), nil
}
