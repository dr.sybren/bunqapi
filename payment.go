/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
)

// Payment is defined by the bunq API.
type Payment struct {
	ID                   int                   `json:"id,omitempty"`
	Created              *Time                 `json:"created,omitempty"`
	Updated              *Time                 `json:"updated,omitempty"`
	Amount               *Amount               `json:"amount,omitempty"`
	CounterpartyAlias    *LabelMonetaryAccount `json:"counterparty_alias,omitempty"`
	Description          string                `json:"description,omitempty"`
	MerchantReference    string                `json:"merchant_reference,omitempty"`
	AllowBunqto          bool                  `json:"allow_bunqto,omitempty"`
	MonetaryAccountID    int                   `json:"monetary_account_id,omitempty"`
	Alias                *LabelMonetaryAccount `json:"alias,omitempty"`
	SubType              string                `json:"sub_type,omitempty"`
	BunqtoStatus         string                `json:"bunqto_status,omitempty"`
	BunqtoSubStatus      string                `json:"bunqto_sub_status,omitempty"`
	BunqtoShareURL       string                `json:"bunqto_share_url,omitempty"`
	BunqtoExpiry         *Time                 `json:"bunqto_expiry,omitempty"`
	BunqtoTimeResponded  *Time                 `json:"bunqto_time_responded,omitempty"`
	BatchID              int                   `json:"batch_id,omitempty"`
	ScheduledID          int                   `json:"scheduled_id,omitempty"`
	AddressShipping      *Address              `json:"address_shipping,omitempty"`
	AddressBilling       *Address              `json:"address_billing,omitempty"`
	Geolocation          *Geolocation          `json:"geolocation,omitempty"`
	AllowChat            bool                  `json:"allow_chat,omitempty"`
	BalanceAfterMutation *Amount               `json:"balance_after_mutation,omitempty"`

	// Not implemented:
	// attachment
	// request_reference_split_the_bill
}

type wrappedPaymentResponse struct {
	Response []Payment `json:"Response"`
}

// CreatePayment performs a payment.
func (c *Client) CreatePayment(fromMonetaryAccountID int, payment Payment) int {
	wrappedResponse := wrappedIDResponse{}
	url := fmt.Sprintf("user/%d/monetary-account/%d/payment", c.session.UserPerson.ID, fromMonetaryAccountID)
	errResp := c.DoRequest("POST", url, &payment, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing payment request")
	}

	response := idResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	log.WithField("paymentID", response.ID.ID).Info("payment performed")
	return response.ID.ID
}

// GetPayment returns info about a payment.
func (c *Client) GetPayment(accountID, paymentID int) (Payment, error) {
	logger := log.WithFields(logrus.Fields{
		"accountID": accountID,
		"paymentID": paymentID,
	})

	wrappedResponse := wrappedPaymentResponse{}
	url := fmt.Sprintf("user/%d/monetary-account/%d/payment/%d", c.session.UserPerson.ID, accountID, paymentID)
	errResp := c.DoRequest("GET", url, nil, &wrappedResponse)
	if errResp != nil {
		logger.WithFields(errResp.LogFields()).Error("error requesting payment")
		return Payment{}, errors.New("error requesting payment")
	}

	payment := Payment{}
	MergeStructs(wrappedResponse.Response, &payment)
	return payment, nil
}
