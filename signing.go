/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/textproto"
	"sort"
	"strings"
)

// ErrNoServerKey is returned when trying to verify a response without the server's RSA key.
var ErrNoServerKey = errors.New("server public RSA key not available")

// The headers to include in the signature. "X-Bunq-..." headers are always included.
var headersToSign = map[string]bool{
	headerCacheControl: true,
	headerUserAgent:    true,
}

func sortedHeaderKeys(header http.Header) []string {
	headerKeys := []string{}
	for key := range header {
		headerKeys = append(headerKeys, key)
	}
	sort.Strings(headerKeys)
	return headerKeys
}

// SignRequest signs a HTTP request using our private RSA key.
func (c *Client) SignRequest(r *http.Request) error {
	if r.Header.Get(headerXBunqClientSignature) != "" {
		log.Panic("this request has already been signed")
	}

	hasher := sha256.New()
	hash := func(value string) {
		// log.WithField("value", value).Debug("writing to hasher")
		hasher.Write([]byte(value))
	}

	hash(fmt.Sprintf("%s %s\n", r.Method, r.URL.Path))
	headerKeys := sortedHeaderKeys(r.Header)
	for _, key := range headerKeys {
		if !headersToSign[key] && !strings.HasPrefix(strings.ToLower(key), "x-bunq-") {
			continue
		}

		for _, value := range r.Header[key] {
			hash(fmt.Sprintf("%s: %s\n", key, value))
		}
	}
	hash("\n")

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Panic("unable to read request body for signing")
	}
	hasher.Write(bodyBytes)
	// log.WithField("value", string(bodyBytes)).Debug("writing body to hasher")

	r.Body = ioutil.NopCloser(bytes.NewReader(bodyBytes))

	sum := hasher.Sum(nil)

	signature, err := rsa.SignPKCS1v15(rand.Reader, c.privateKey, crypto.SHA256, sum)
	if err != nil {
		log.WithError(err).Fatal("unable to sign with private RSA key")
	}

	// Verify the signature just to be sure ;-)
	if err := rsa.VerifyPKCS1v15(c.publicKey, crypto.SHA256, sum, signature); err != nil {
		log.WithError(err).Fatal("unable to verify signature with public RSA key")
	}

	encodedSig := base64.StdEncoding.EncodeToString(signature)
	// log.WithFields(logrus.Fields{
	// 	"shaSum":    fmt.Sprintf("%x", sum),
	// 	"signature": encodedSig,
	// }).Debug("calculated signature")

	r.Header.Set(headerXBunqClientSignature, encodedSig)
	return nil
}

// VerifyResponse verifies a HTTP response using the server's public RSA key.
// We obtained the server's key in the PostInstall() function.
func (c *Client) VerifyResponse(r *http.Response) error {
	if c.creds.serverPublicKey == nil {
		return ErrNoServerKey
	}

	// TODO: merge common code between this function and SignRequest().
	serverSignatureB64 := r.Header.Get(headerXBunqServerSignature)
	if serverSignatureB64 == "" {
		log.Panic("this response has not been signed")
	}

	hasher := sha256.New()
	hash := func(value string) {
		// log.WithField("value", value).Debug("writing to hasher")
		hasher.Write([]byte(value))
	}

	hash(fmt.Sprintf("%d\n", r.StatusCode))
	headerKeys := sortedHeaderKeys(r.Header)
	serverSigLowerKey := strings.ToLower(headerXBunqServerSignature)
	for _, key := range headerKeys {
		lowerKey := strings.ToLower(key)
		if !strings.HasPrefix(lowerKey, "x-bunq-") || lowerKey == serverSigLowerKey {
			continue
		}

		for _, value := range r.Header[key] {
			hash(fmt.Sprintf("%s: %s\n", textproto.CanonicalMIMEHeaderKey(key), value))
		}
	}
	hash("\n")

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Panic("unable to read response body for signing")
	}
	hasher.Write(bodyBytes)
	// log.WithField("value", string(bodyBytes)).Debug("writing body to hasher")

	r.Body = ioutil.NopCloser(bytes.NewReader(bodyBytes))
	sum := hasher.Sum(nil)

	// Verify the signature.
	serverSignature, err := base64.StdEncoding.DecodeString(serverSignatureB64)
	if err != nil {
		log.WithError(err).Fatal("unable to base64-decode the server signature")
	}
	if err := rsa.VerifyPKCS1v15(c.creds.serverPublicKey, crypto.SHA256, sum, serverSignature); err != nil {
		log.WithError(err).Fatal("unable to verify server signature with public RSA key")
	}

	// encodedSig := base64.StdEncoding.EncodeToString(serverSignature)
	// log.WithFields(logrus.Fields{
	// 	"shaSum":    fmt.Sprintf("%x", sum),
	// 	"signature": encodedSig,
	// }).Debug("verified server response signature")

	return nil
}
