/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

// BunqID wraps an ID
type BunqID struct {
	ID int `json:"id"`
}

type wrappedIDResponse struct {
	Response []idResponse `json:"Response"`
}
type idResponse struct {
	ID *BunqID `json:"Id,omitempty"`
}

// Geolocation is defined by the bunq API.
type Geolocation struct {
	Latitude  int `json:"latitude"`
	Longitude int `json:"longitude"`
	Altitude  int `json:"altitude"`
	Radius    int `json:"radius"`
}

// Pointer is defined by the bunq API.
type Pointer struct {
	Type  string `json:"type,omitempty"`
	Value string `json:"value,omitempty"`
	Name  string `json:"name,omitempty"`
}

// ErrorResponse is defined by the bunq API.
type ErrorResponse struct {
	Error []struct {
		Description string `json:"error_description"`
		Translated  string `json:"error_description_translated"`
	} `json:"Error"`
}

// LogFields returns logrus fields that represent this error response.
func (er *ErrorResponse) LogFields() logrus.Fields {
	switch len(er.Error) {
	case 0:
		return logrus.Fields{"errorDescription": "-missing-"}
	case 1:
		return logrus.Fields{
			"errorDescription": er.Error[0].Description,
			"errorTranslated":  er.Error[0].Translated,
		}
	default:
		f := logrus.Fields{}
		for index, err := range er.Error {
			f[fmt.Sprintf("errorDescription%d", index)] = err.Description
			f[fmt.Sprintf("errorTranslated%d", index)] = err.Translated
		}
		return f
	}
}
