/* (c) 2019 David Stotijn, dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

// Alias is defined by the bunq API.
type Alias struct {
	Type  string `json:"type"`
	Value string `json:"value"`
	Name  string `json:"name"`
}

// AvatarImage is defined by the bunq API.
type AvatarImage struct {
	AttachmentPublicUUID string `json:"attachment_public_uuid"`
	ContentType          string `json:"content_type"`
	Height               int    `json:"height"`
	Width                int    `json:"width"`
}

// UBO is defined by the bunq API.
type UBO struct {
	Name        string `json:"name"`
	DateOfBirth Time   `json:"date_of_birth"`
	Nationality string `json:"nationality"`
}

// NotificationFilter is defined by the bunq API.
type NotificationFilter struct {
	NotificationDeliveryMethod string `json:"notification_delivery_method"`
	NotificationTarget         string `json:"notification_target"`
	Category                   string `json:"category"`
}

// Address is defined by the bunq API.
type Address struct {
	Street      string `json:"street"`
	HouseNumber string `json:"house_number"`
	POBox       string `json:"po_box"`
	PostalCode  string `json:"postal_code"`
	City        string `json:"city"`
	Country     string `json:"country"`
}

// Avatar is defined by the bunq API.
type Avatar struct {
	UUID       string        `json:"uuid"`
	AnchorUUID string        `json:"anchor_uuid"`
	Image      []AvatarImage `json:"image"`
}

// DirectorAlias is defined by the bunq API.
type DirectorAlias struct {
	UUID           string `json:"uuid"`
	Avatar         Avatar `json:"avatar"`
	PublicNickName string `json:"public_nick_name"`
	DisplayName    string `json:"display_name"`
	Country        string `json:"country"`
}

// Limit is defined by the bunq API.
type Limit struct {
	Value    string `json:"value"`
	Currency string `json:"currency"`
}

// UserCompany is defined by the bunq API.
type UserCompany struct {
	ID                                 int                  `json:"id"`
	CreatedAt                          Time                 `json:"created"`
	UpdatedAt                          Time                 `json:"updated"`
	PublicUUID                         string               `json:"public_uuid"`
	Name                               string               `json:"name"`
	DisplayName                        string               `json:"display_name"`
	PublicNickName                     string               `json:"public_nick_name"`
	Alias                              []Alias              `json:"alias"`
	ChamberOfCommerceNumber            string               `json:"chamber_of_commerce_number"`
	TypeOfBusinessEntity               string               `json:"type_of_business_entity"`
	SectorOfIndustry                   string               `json:"sector_of_industry"`
	CounterBankIBAN                    string               `json:"counter_bank_IBAN"`
	Avatar                             Avatar               `json:"avatar"`
	AddressMain                        Address              `json:"address_main"`
	AddressPostal                      Address              `json:"address_postal"`
	VersionTermsOfService              string               `json:"version_terms_of_service"`
	DirectorAlias                      DirectorAlias        `json:"director_alias"`
	Language                           string               `json:"language"`
	Region                             string               `json:"region"`
	UBO                                []UBO                `json:"UBO"`
	Status                             string               `json:"status"`
	SubStatus                          string               `json:"sub_status"`
	SessionTimeout                     int                  `json:"session_timeout"`
	DailyLimitWithoutConfirmationLogin Limit                `json:"daily_limit_without_confirmation_login"`
	NotificationFilters                []NotificationFilter `json:"notification_filters"`
}

// UserPerson is defined by the bunq API.
type UserPerson struct {
	ID      int  `json:"id"`
	Created Time `json:"created"`
	Updated Time `json:"updated"`

	FirstName                          string               `json:"first_name"`
	MiddleName                         string               `json:"middle_name"`
	LastName                           string               `json:"last_name"`
	PublicNickName                     string               `json:"public_nick_name"`
	AddressMain                        Address              `json:"address_main"`
	AddressPostal                      Address              `json:"address_postal"`
	AvatarUUID                         string               `json:"avatar_uuid"`
	TaxResident                        string               `json:"tax_resident"`
	DocumentType                       string               `json:"document_type"`
	DocumentNumber                     string               `json:"document_number"`
	DocumentCountryOfIssuance          string               `json:"document_country_of_issuance"`
	DocumentFrontAttachmentID          int                  `json:"document_front_attachment_id"`
	DocumentBackAttachmentID           int                  `json:"document_back_attachment_id"`
	DateOfBirth                        string               `json:"date_of_birth"`
	PlaceOfBirth                       string               `json:"place_of_birth"`
	CountryOfBirth                     string               `json:"country_of_birth"`
	Nationality                        string               `json:"nationality"`
	Language                           string               `json:"language"`
	Region                             string               `json:"region"`
	Gender                             string               `json:"gender"`
	Status                             string               `json:"status"`
	SubStatus                          string               `json:"sub_status"`
	LegalGuardianAlias                 []Alias              `json:"legal_guardian_alias"`
	SessionTimeout                     int                  `json:"session_timeout"`
	DailyLimitWithoutConfirmationLogin Amount               `json:"daily_limit_without_confirmation_login"`
	NotificationFilters                []NotificationFilter `json:"notification_filters"`
	DisplayName                        string               `json:"display_name"`
	PublicUUID                         string               `json:"public_uuid"`
	LegalName                          string               `json:"legal_name"`
	Alias                              []Alias              `json:"alias"`
	Avatar                             Avatar               `json:"avatar"`
	VersionTermsOfService              string               `json:"version_terms_of_service"`
}

// LabelUser is defined by the bunq API.
type LabelUser struct {
	UUID           string  `json:"uuid,omitempty"`
	DisplayName    string  `json:"display_name,omitempty"`
	Country        string  `json:"country,omitempty"`
	Avatar         *Avatar `json:"avatar,omitempty"`
	PublicNickName string  `json:"public_nick_name,omitempty"`
}
