/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/bunqapi"
)

func main() {
	logfmt := &logrus.TextFormatter{
		ForceColors: true,
	}
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(logfmt)
	bunqapi.LogLevel(logrus.GetLevel())
	bunqapi.LogFormatter(logfmt)

	creds := bunqapi.LoadCredentials()
	logrus.WithField("apiMode", creds.APIMode).Info("loaded credentials")

	client := bunqapi.NewClient(creds)
	client.CheckInstallation()
	client.CheckDeviceServer("je moeder", []string{
		"62.251.18.36",
		"2001:984:42c1:1:712e:7ce7:8e46:1fdb",
	})
	client.SessionStart()
	defer client.SessionStop()

	// Find accounts to toy with.
	accounts := client.GetMonetaryAccountBankList()
	for len(accounts) < 2 {
		logrus.Warning("not enough bank accounts, creating one")

		bankAccount := bunqapi.MonetaryAccountBank{
			Currency: "EUR",
		}
		client.CreateMonetaryAccountBank(bankAccount)

		accounts = client.GetMonetaryAccountBankList()
	}
	logrus.WithField("numAccounts", len(accounts)).Info("enough bank accounts found")

	// Request some money
	amount := bunqapi.NewAmount("EUR", 30000)
	rfp := bunqapi.RequestInquiry{
		AmountInquired: &amount,
		Description:    "Daddy daddy can I play?",
		AllowBunqme:    true,
		CounterpartyAlias: &bunqapi.LabelMonetaryAccount{
			Type:  "EMAIL",
			Value: "sugardaddy@bunq.com",
		},
	}
	client.CreateRequestInquiry(accounts[1].ID, rfp)

	for _, account := range accounts {
		logrus.WithFields(account.AliasFields()).WithFields(logrus.Fields{
			"accountID": account.ID,
			"balance":   account.Balance.String(),
		}).Info("account balance")
	}
}
