/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"github.com/sirupsen/logrus"
)

// InstallationConfig contains our installation token and the server's public RSA key.
type InstallationConfig struct {
	Token           string `yaml:"token,omitempty"`
	ServerPublicKey string `yaml:"serverPublicKey,omitempty"`
}

type installationRequest struct {
	ClientPublicKey string `json:"client_public_key"`
}

type wrappedInstallationResponse struct {
	Response []installationResponse `json:"Response"`
}

type installationResponse struct {
	ID *BunqID `json:"Id"`

	Token *struct {
		ID      int    `json:"id"`
		Created string `json:"created"`
		Updated string `json:"updated"`
		Token   string `json:"token"`
	} `json:"Token"`

	ServerPublicKey *struct {
		Key string `json:"server_public_key"`
	} `json:"ServerPublicKey"`
}

func publicKeyString(privateKey *rsa.PrivateKey) string {
	if privateKey == nil {
		log.Panic("private key cannot be nil")
	}
	pubKeyDer, err := x509.MarshalPKIXPublicKey(privateKey.Public())
	if err != nil {
		log.WithError(err).Fatal("error serializing public key to DER-encoded PKIX format")
	}
	pubKeyPemBlock := pem.Block{
		Type:    "PUBLIC KEY",
		Headers: nil,
		Bytes:   pubKeyDer,
	}
	pubKeyPem := pem.EncodeToMemory(&pubKeyPemBlock)
	return string(pubKeyPem)
}

// PostInstallation performs a POST to the 'installation' endpoint
func (c *Client) PostInstallation() {
	payload := installationRequest{
		ClientPublicKey: publicKeyString(c.privateKey),
	}

	wrappedResponse := wrappedInstallationResponse{}
	errResp := c.DoRequest("POST", "installation", &payload, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing installation request")
	}

	response := installationResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	c.creds.ServerPublicKey = response.ServerPublicKey.Key
	c.creds.InstallationToken = response.Token.Token
	c.creds.updateServerPublicKey()

	log.WithFields(logrus.Fields{
		"installationToken": c.creds.InstallationToken,
		"serverPublicKey":   c.creds.ServerPublicKey,
	}).Info("installation token obtained")

	c.creds.Save()
}

// CheckInstallation performs a POST to the 'installation' endpoint if we have no installation token or server RSA key.
func (c *Client) CheckInstallation() {
	if c.creds.InstallationToken != "" && c.creds.ServerPublicKey != "" {
		log.WithFields(logrus.Fields{
			"installationToken": c.creds.InstallationToken,
		}).Debug("installation token present")
		return
	}
	c.PostInstallation()
}
