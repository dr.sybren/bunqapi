/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
)

type wrappedMonetaryAccountBankResponse struct {
	Response []monetaryAccountBankResponse `json:"Response"`
}
type monetaryAccountBankResponse struct {
	MonetaryAccountBank MonetaryAccountBank `json:"MonetaryAccountBank"`
}

// MonetaryAccountBank is defined by the bunq API.
type MonetaryAccountBank struct {
	ID                  int                     `json:"id,omitempty"`
	Created             *Time                   `json:"created,omitempty"`
	Updated             *Time                   `json:"updated,omitempty"`
	Avatar              *Avatar                 `json:"avatar,omitempty"`
	Currency            string                  `json:"currency"`
	Description         string                  `json:"description,omitempty"`
	DailyLimit          *Amount                 `json:"daily_limit,omitempty"`
	DailySpent          *Amount                 `json:"daily_spent,omitempty"`
	OverdraftLimit      *Amount                 `json:"overdraft_limit,omitempty"`
	Balance             *Amount                 `json:"balance,omitempty"`
	Alias               []Alias                 `json:"alias,omitempty"`
	PublicUUID          string                  `json:"public_uuid,omitempty"`
	Status              string                  `json:"status,omitempty"`
	SubStatus           string                  `json:"sub_status,omitempty"`
	Reason              string                  `json:"reason,omitempty"`
	ReasonDescription   string                  `json:"reason_description,omitempty"`
	UserID              int                     `json:"user_id,omitempty"`
	NotificationFilters []NotificationFilter    `json:"notification_filters,omitempty"`
	Setting             *MonetaryAccountSetting `json:"setting,omitempty"`
	// monetary_account_profile is not yet implemented
}

// MonetaryAccountSetting is defined by the bunq API.
type MonetaryAccountSetting struct {
	Color               string `json:"color"`
	DefaultAvatarStatus string `json:"default_avatar_status"`
	RestrictionChat     string `json:"restriction_chat"`
}

// IBANAlias returns the IBAN alias of the bank account.
func (mab MonetaryAccountBank) IBANAlias() Alias {
	for idx := range mab.Alias {
		if mab.Alias[idx].Type == "IBAN" {
			return mab.Alias[idx]
		}
	}

	return Alias{}
}

// AliasFields returns a log field for each alias of the account.
func (mab MonetaryAccountBank) AliasFields() logrus.Fields {
	fields := logrus.Fields{}
	for _, alias := range mab.Alias {
		fields[alias.Type] = alias.Value
	}
	return fields
}

// GetMonetaryAccountBankList fetches a list of the current user's own bank accounts.
func (c *Client) GetMonetaryAccountBankList() []MonetaryAccountBank {
	wrappedResponse := wrappedMonetaryAccountBankResponse{}
	url := fmt.Sprintf("user/%d/monetary-account-bank", c.session.UserPerson.ID)
	errResp := c.DoRequest("GET", url, nil, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error requesting bank accounts")
	}

	accounts := []MonetaryAccountBank{}
	for index := range wrappedResponse.Response {
		accounts = append(accounts, wrappedResponse.Response[index].MonetaryAccountBank)
	}

	return accounts
}

// GetMonetaryAccountBank returns a single bank account.
func (c *Client) GetMonetaryAccountBank(accountID int) (MonetaryAccountBank, error) {
	logger := log.WithField("accountID", accountID)

	wrappedResponse := wrappedMonetaryAccountBankResponse{}
	url := fmt.Sprintf("user/%d/monetary-account-bank/%d", c.session.UserPerson.ID, accountID)
	errResp := c.DoRequest("GET", url, nil, &wrappedResponse)
	if errResp != nil {
		logger.WithFields(errResp.LogFields()).Error("error requesting bank account")
		return MonetaryAccountBank{}, errors.New("error requesting bank account")
	}

	accounts := []MonetaryAccountBank{}
	for index := range wrappedResponse.Response {
		accounts = append(accounts, wrappedResponse.Response[index].MonetaryAccountBank)
	}

	return accounts[0], nil
}

// CreateMonetaryAccountBank creates a new bank account for the current user.
func (c *Client) CreateMonetaryAccountBank(account MonetaryAccountBank) {
	wrappedResponse := wrappedIDResponse{}
	url := fmt.Sprintf("user/%d/monetary-account-bank", c.session.UserPerson.ID)
	errResp := c.DoRequest("POST", url, &account, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing session creation request")
	}

	response := idResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	log.WithField("bankAccountID", response.ID.ID).Info("bank account created")
}
