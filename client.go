/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"bytes"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// Client is the bunq API client.
type Client struct {
	creds   Credentials
	session Session

	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey

	httpClient *http.Client
}

// NewClient creates a new Client using the given credentials.
func NewClient(creds Credentials) *Client {
	privateKey := ensureOwnRSAKey()
	publicKey := privateKey.Public()
	publicRSAKey, ok := publicKey.(*rsa.PublicKey)
	if !ok {
		log.WithField("keyType", fmt.Sprintf("%T", publicKey)).Fatal("expected public key of RSA type")
	}

	return &Client{
		creds:      creds,
		session:    Session{},
		privateKey: privateKey,
		publicKey:  publicRSAKey,
		httpClient: &http.Client{
			Timeout: 15 * time.Minute,
		},
	}
}

func (c *Client) newRequest(method, endpoint string, payload interface{}) *http.Request {
	url := c.creds.Endpoint(endpoint)

	var body []byte
	var err error
	if payload != nil {
		body, err = json.Marshal(payload)
		if err != nil {
			log.WithFields(logrus.Fields{
				"url":           url,
				"payload":       payload,
				logrus.ErrorKey: err,
			}).Panic("unable to marshal payload as JSON")
		}
		// fmt.Printf("\n%s\n\n", string(body))
	}

	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		log.WithFields(logrus.Fields{
			"url":           url,
			logrus.ErrorKey: err,
		}).Panic("unable to create HTTP request")
	}

	req.Header.Set(headerAccept, headerAcceptJSON)
	req.Header.Set(headerCacheControl, headerCacheControlNone)
	req.Header.Set(headerUserAgent, headerUserAgentAPI)
	req.Header.Set(headerXBunqGeolocation, headerGeolocationZero)
	req.Header.Set(headerXBunqLanguage, headerLanguageNederlands)
	req.Header.Set(headerXBunqRegion, headerRegionNL)
	req.Header.Set(headerXBunqClientRequestID, time.Now().String())

	switch endpoint {
	case "installation":
	case "device-server", "session-server":
		req.Header.Set(headerXBunqClientAuthentication, c.creds.InstallationToken)
	default:
		req.Header.Set(headerXBunqClientAuthentication, c.session.Token)
	}

	return req
}

// DoRequest performs a HTTP request.
func (c *Client) DoRequest(method, endpoint string, payload interface{}, response interface{}) *ErrorResponse {
	logger := log.WithFields(logrus.Fields{
		"method":   method,
		"endpoint": endpoint,
	})

	req := c.newRequest(method, endpoint, payload)
	logger = logger.WithField("url", req.URL)
	c.SignRequest(req)

	logger.Debug("performing request")
	resp, err := c.httpClient.Do(req)
	if err != nil {
		logger.WithError(err).Fatal("unable to perform HTTP call")
	}

	// We can only verify the response when we have an RSA key, and we get that through the 'installation' call.
	switch endpoint {
	case "installation":
		logger.Debug("accepting server response without ability to verify")
	default:
		if err := c.VerifyResponse(resp); err != nil {
			logger.WithError(err).Fatal("unable to verify server response")
		}
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.WithError(err).Fatal("unable to read HTTP body")
	}

	contentType := resp.Header.Get(headerContentType)
	if contentType != "application/json" {
		fields := logrus.Fields{
			"contentType": contentType,
			"body":        string(respBody),
		}
		for headerKey := range resp.Header {
			fields["http-"+headerKey] = resp.Header.Get(headerKey)
		}
		logger.WithFields(fields).Fatal("invalid content type received")
	}

	logger = logger.WithField("status", resp.StatusCode)
	if resp.StatusCode != 200 {
		errorResponse := ErrorResponse{}
		if err := json.Unmarshal(respBody, &errorResponse); err != nil {
			logger.WithFields(logrus.Fields{
				logrus.ErrorKey: err,
				"body":          string(respBody),
			}).Fatal("unable to parse error response JSON")
		}
		return &errorResponse
	}

	if response != nil {
		if err := json.Unmarshal(respBody, response); err != nil {
			logger.WithFields(logrus.Fields{
				logrus.ErrorKey: err,
				"body":          string(respBody),
			}).Fatal("unable to parse response JSON")
		}

		// fmt.Println("\nBODY:\n" + string(respBody))
		// fmt.Println("\nParsed and then as JSON:")
		// asJSON, err := json.MarshalIndent(response, "", "    ")
		// if err != nil {
		// 	panic(err)
		// }
		// os.Stdout.Write(asJSON)
		// fmt.Println()

		// logger.WithField("body", string(respBody)).Info("response")
	}

	return nil
}
