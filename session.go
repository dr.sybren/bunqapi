/* (c) 2019 dr. Sybren A. Stüvel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bunqapi

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

// Session stores session information the client.
type Session struct {
	ID          int
	Token       string
	UserCompany *UserCompany
	UserPerson  *UserPerson
}

type sessionServerRequest struct {
	// The API key.
	Secret string `json:"secret"`
}

type wrappedSessionServerResponse struct {
	Response []sessionServerResponse `json:"Response"`
}
type sessionServerResponse struct {
	ID    *BunqID `json:"Id"`
	Token *struct {
		ID    int    `json:"id"`
		Token string `json:"token"`
	} `json:"Token"`

	UserCompany *UserCompany `json:"UserCompany"`
	UserPerson  *UserPerson  `json:"UserPerson"`
}

// SessionStart creates a bunq session. It will be used in subsequent API calls.
func (c *Client) SessionStart() {
	payload := sessionServerRequest{
		Secret: c.creds.APIKey,
	}

	wrappedResponse := wrappedSessionServerResponse{}
	errResp := c.DoRequest("POST", "session-server", &payload, &wrappedResponse)
	if errResp != nil {
		log.WithFields(errResp.LogFields()).Fatal("error performing session creation request")
	}

	response := sessionServerResponse{}
	MergeStructs(wrappedResponse.Response, &response)

	c.session.ID = response.ID.ID
	c.session.Token = response.Token.Token
	c.session.UserCompany = response.UserCompany
	c.session.UserPerson = response.UserPerson

	log.WithFields(logrus.Fields{
		"firstName": c.session.UserPerson.FirstName,
		"lastName":  c.session.UserPerson.LastName,
		"session":   c.session.Token,
	}).Debug("session created")
}

// SessionStop closes the current bunq session.
func (c *Client) SessionStop() {
	logger := log.WithField("sessionID", c.session.ID)
	logger.Debug("closing session")

	url := fmt.Sprintf("session/%d", c.session.ID)
	errResp := c.DoRequest("DELETE", url, nil, nil)
	if errResp != nil {
		logger.WithFields(errResp.LogFields()).Error("error performing session deletion request")
	}
}
